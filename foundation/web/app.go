package web

import (
	"context"
	"fmt"
	"net/http"
)

type Handler func(ctx context.Context, w http.ResponseWriter, r *http.Request) error

type App struct {
	mux *http.ServeMux
}

func NewApp() *App {
	mux := http.NewServeMux()
	return &App{mux}
}

func (a *App) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	a.mux.ServeHTTP(w, r)
}

func (a *App) Handle(method string, group string, path string, handler Handler, mw ...Middleware) {
	handler = wrapMiddleware(mw, handler)
	finalPath := path
	if group != "" {
		finalPath = "/" + group + path
	}
	h := func(w http.ResponseWriter, r *http.Request) {
		if r.Method != method {
			return
		}
		ctx := r.Context()
		if err := handler(ctx, w, r); err != nil {
			fmt.Printf("handle error: [%s] [%s] %s\n", method, finalPath, err)
			return
		}
	}

	fmt.Printf("handle: [%s] %s\n", method, finalPath)
	a.mux.HandleFunc(finalPath, h)
}
