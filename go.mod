module gitlab.com/dynamap/web

go 1.20

require gitlab.com/dynamap/dynamap v0.6.0

require golang.org/x/mod v0.10.0 // indirect
