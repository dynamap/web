package start

import (
	"context"
	"encoding/json"
	"fmt"
	"gitlab.com/dynamap/dynamap"
	"{{.GoModule}}/business/processes/{{.GoPackage}}"
	"net/http"
)

type ProcessInstanceStartRequest struct {
	Id   string
	Data {{.GoPackage}}.{{.GoStartVariableType}}
}

type Start{{.Id}}Handler struct {
	Engine *dynamap.BPMNEngine
}

func (s Start{{.Id}}Handler) StartProcess(ctx context.Context, w http.ResponseWriter, req *http.Request) error {
	var startRequest ProcessInstanceStartRequest
	err := json.NewDecoder(req.Body).Decode(&startRequest)
	if err != nil {
		fmt.Println(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return err
	}

	id := s.Engine.GenerateId(dynamap.NoneStartEventType)
	{{$gopkg := .GoPackage}}
	{{with index .StartEvents 0}}startEvent := dynamap.NewNoneStartEvent(startRequest.Id, "", {{$gopkg}}.{{.Id}}, id){{end}}
	p := {{.GoPackage}}.New{{.Id}}(startRequest.Id, s.Engine, startEvent, startRequest.Data)
	s.Engine.SendCommand(dynamap.StartProcessInstanceCmd{Instance: p})
	return nil
}