package main

import (
	"bytes"
	_ "embed"
	"flag"
	"fmt"
	"gitlab.com/dynamap/dynamap/builder"
	"go/format"
	"log"
	"os"
	"os/exec"
	"path"
	"path/filepath"
	"strings"
	"text/template"
)

//go:embed "templates/processhandlers.got"
var gotProcessHandlers string

//go:embed "templates/startprocesshandler.got"
var gotStartProcessHandler string

//go:embed "templates/completetaskhandler.got"
var gotCompleteTaskHandler string

//go:embed "templates/main.got"
var gotMain string

//go:embed "templates/service.got"
var gotService string

//go:embed "templates/starters.got"
var gotStarters string

//go:embed "templates/timerstarter.got"
var gotTimerStarter string

var input = flag.String("input", ".", "input bpmn file or directory containing bpmn files")
var output = flag.String("output", ".", "output directory for code")

func main() {
	flag.Parse()

	if len(flag.Args()) > 0 {
		cmd := flag.Args()[0]
		switch {
		case cmd == "init":
			initApp()
		case cmd == "gen":
			genBPMN()
		case cmd == "help":
			printUsage()
		default:
			fmt.Printf("%s: unknown command\nRun 'buildapp help' for usage.", cmd)
			printUsage()
		}
	} else {
		printUsage()
	}
}

func printUsage() {
	fmt.Println("Commands: ")
	fmt.Println("  init : initializes a new process project and generates go code for a process")
	fmt.Println("  gen : generates go code for a process")
	flag.PrintDefaults()
}

func initApp() {
	fmt.Println("Initializing app...")
	genBPMNProject()
	fmt.Println("App initialized.")
}

func createDirectories(dirs ...string) error {
	for _, newDir := range dirs {
		err := os.MkdirAll(newDir, os.ModePerm)
		if err != nil {
			return err
		}
	}
	return nil
}

func generateOutputFileName(inputFile string, process *builder.XMLBPMNProcess, input, output string, isOutputDir bool) string {
	packageName := process.GoPackage
	outputFile := output
	if isOutputDir {
		inputFileName := filepath.Base(inputFile)
		outputFileName := inputFileName[:len(inputFileName)-len(filepath.Ext(inputFileName))] + "_gen.go"
		outputFile = filepath.Join(outputFile, "business", "processes", packageName)
		if err := createDirectories(outputFile); err != nil {
			log.Println(err)
		}
		outputFile = filepath.Join(outputFile, outputFileName)
	}
	return outputFile
}

func genBPMNProject() {
	dir, err := os.Getwd()
	if err != nil {
		fmt.Printf("error getting current directory: %v\n", err)
	}
	*input = filepath.Join(dir, *input)
	*output = filepath.Join(dir, *output)
	fmt.Printf("Processing input: '%s'\n", *input)
	pg := builder.NewProcessGenerator(*input, *output)
	pg.OutputFileNameGenerator = generateOutputFileName

	err = pg.GenerateAndWriteCodeFromBPMN()
	if err != nil {
		fmt.Printf("error: '%v'", err)
		return
	}

	// Generate Process Handlersx
	err = generateProcessHandlers(pg)
	if err != nil {
		log.Fatal(err)
	}

	// Generate Start Handlers
	err = genTemplateFromProcessWithProcessIdFileName(pg, "StartProcessHandler", gotStartProcessHandler, filepath.Join(*output, "./app/service/handlers/start"))
	if err != nil {
		log.Fatal(err)
	}

	// Generate Complete Task Handlers
	err = genTemplateFromProcessWithProcessIdFileName(pg, "CompleteTaskHandler", gotCompleteTaskHandler, filepath.Join(*output, "./app/service/handlers/complete"))
	if err != nil {
		log.Fatal(err)
	}

	// Generate Start Event Processors
	err = generateStartEventProcessors(pg)
	if err != nil {
		log.Fatal(err)
	}

	// Generate Starters list
	err = genTemplateFromProcess(pg, "Starters", gotStarters, filepath.Join(*output, "./app/service/starters"), "starters.go")
	if err != nil {
		log.Fatal(err)
	}

	// Generate main
	if !fileExists(filepath.Join(*output, "./app/service/main.go")) {
		err = genTemplate("Main", gotMain, filepath.Join(*output, "./app/service/"), "main.go")
		if err != nil {
			log.Fatal(err)
		}
	}

	// Generate service
	err = genTemplate("Service", gotService, filepath.Join(*output, "./app/service/web/"), "service.go")
	if err != nil {
		log.Fatal(err)
	}

	// Format/update imports
	runGoImport(".")
}

func fileExists(path string) bool {
	_, err := os.Stat(path)
	return err == nil
}

func runGoImport(path string) error {
	fmt.Printf("Running goimports... ")
	cmd := exec.Command("goimports", "-w", path)
	err := cmd.Run()
	if err != nil {
		fmt.Printf("error\n")
		return err
	}
	fmt.Printf("complete.\n")
	return nil
}

func generateProcessHandlers(pg *builder.ProcessGenerator) error {
	var process builder.XMLBPMNProcess
	for _, gen := range pg.OutputGen {
		process = gen.Defs.Process
		break
	}
	var temp *template.Template
	temp, err := template.New("ProcessHandlers").Parse(gotProcessHandlers)
	if err != nil {
		return err
	}
	var buf bytes.Buffer
	err = temp.Execute(&buf, process)
	if err != nil {
		return err
	}
	bytes, err := format.Source([]byte(buf.String()))
	if err != nil {
		return err
	}
	err = createDirectories(filepath.Join(pg.Output, "./app/service/handlers"))
	if err != nil {
		return err
	}
	file := filepath.Join(pg.Output, "./app/service/handlers/handlers.go")
	err = os.WriteFile(file, bytes, 0666)
	return err
}

func generateStartEventProcessors(pg *builder.ProcessGenerator) error {
	var process builder.XMLBPMNProcess
	for _, gen := range pg.OutputGen {
		process = gen.Defs.Process
		break
	}
	for _, se := range process.StartEvents {
		if se.TimerEventDefinition.TimeCycle == "" {
			return nil
		}
	}
	var temp *template.Template
	temp, err := template.New("TimerStarter").Parse(gotTimerStarter)
	if err != nil {
		return err
	}
	var buf bytes.Buffer
	err = temp.Execute(&buf, process)
	if err != nil {
		return err
	}
	bytes, err := format.Source([]byte(buf.String()))
	if err != nil {
		return err
	}
	err = createDirectories(filepath.Join(pg.Output, "./app/service/starters"))
	if err != nil {
		return err
	}
	file := filepath.Join(pg.Output, "./app/service/starters/timer.go")
	err = os.WriteFile(file, bytes, 0666)
	return err
}

func genTemplateFromProcessWithProcessIdFileName(pg *builder.ProcessGenerator, name, text, outputDir string) error {
	var process builder.XMLBPMNProcess
	for _, gen := range pg.OutputGen {
		process = gen.Defs.Process
		break
	}
	outputFile := strings.ToLower(process.Id) + ".go"
	return genTemplateFromProcess(pg, name, text, outputDir, outputFile)
}

func genTemplateFromProcess(pg *builder.ProcessGenerator, name, text, outputDir, outputFile string) error {
	var process builder.XMLBPMNProcess
	for _, gen := range pg.OutputGen {
		process = gen.Defs.Process
		break
	}
	var temp *template.Template
	temp, err := template.New(name).Parse(text)
	if err != nil {
		return err
	}
	var buf bytes.Buffer
	err = temp.Execute(&buf, process)
	if err != nil {
		return err
	}
	bytes, err := format.Source([]byte(buf.String()))
	if err != nil {
		return err
	}
	err = createDirectories(outputDir)
	if err != nil {
		return err
	}
	err = os.WriteFile(path.Join(outputDir, outputFile), bytes, 0666)
	return err
}

func genTemplate(name, text, outputDir, outputFile string) error {
	var temp *template.Template
	temp, err := template.New(name).Parse(text)
	if err != nil {
		return err
	}
	var buf bytes.Buffer
	err = temp.Execute(&buf, nil)
	if err != nil {
		return err
	}
	bytes, err := format.Source([]byte(buf.String()))
	if err != nil {
		return err
	}
	err = createDirectories(outputDir)
	if err != nil {
		return err
	}
	err = os.WriteFile(path.Join(outputDir, outputFile), bytes, 0666)
	return err
}

func genBPMN() {
	if !builder.IsBPMN(*input) && !builder.IsDir(*input) {
		fmt.Printf("input '%s' must be a directory or file ending with .bpmn\n", *input)
		return
	}
	dir, err := os.Getwd()
	if err != nil {
		fmt.Printf("error getting current directory: %v\n", err)
	}
	*input = filepath.Join(dir, *input)
	*output = filepath.Join(dir, *output)
	fmt.Printf("Processing input: '%s'\n", *input)
	pg := builder.NewProcessGenerator(*input, *output)
	err = pg.GenerateAndWriteCodeFromBPMN()
	if err != nil {
		fmt.Printf("error: '%v'", err)
		return
	}
}
