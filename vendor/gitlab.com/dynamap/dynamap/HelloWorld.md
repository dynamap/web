# Hello World BPMN Process
The following example creates a command line application that executes a Hello World process.

![example/helloworld.bpmn](example/helloworld.svg)

The process starts with a data input `Name`, and uses a BPMN script task to print to the console `Hello [Name]`.

The script uses the go `fmt.Printf` function to print to the console, referencing the process data element `Name`.

![example/script.png](example/script.png)

1. Initialize a new go module for the app:
```shell
mkdir helloworldapp
cd helloworldapp
go mod init helloworldapp
```
2. Add dynamap to go dependencies:

```shell
go get gitlab.com/dynamap/dynamap
```

3. Install the dynamap command line builder:

```shell
go install gitlab.com/dynamap/dynamap/builder/cmd/dynamap
```

4. Make a folder for the helloworld package:
```shell
mkdir helloworld
cd helloworld
```
5. Copy the example .bpmn process from [example/helloworld.bpmn](example/helloworld.bpmn) to the helloworld package folder.
6. Generate the go code for the process, which creates `helloworld_gen.go`.
```shell
go run gitlab.com/dynamap/dynamap/builder/cmd/dynamap
```
7. Create `helloworld.go` with a Data type for the process to start with:

```go
package helloworld

// Data holds the process data for the helloworld.bpmn process
type Data struct {
	Name string // Name contains the name to say hello to from the script task
}
```
8. Go back to the main app folder:
```shell
cd ..
```
9. Create `main.go` to test execution of the process:
```go
package main

import (
	"gitlab.com/dynamap/dynamap"
	"helloworldapp/helloworld"
	"log"
)

func main() {
	// Create the engine
	engine := dynamap.NewMemoryBPMNEngine()

	// Start the engine
	if err := engine.Start(); err != nil {
		log.Fatal(err)
	}

	defer func() {
		// Wait for running process to complete
		engine.Wait()

		// Shut down the engine
		engine.Stop()
	}()

	// Create a new process instance with an id, start event, and input
	id := engine.GenerateId(dynamap.ProcessInstanceType)
	start := dynamap.NewNoneStartEvent("StartHelloWorldEvent", engine.IdGenerator.GenerateId(dynamap.NoneStartEventType))
	input := helloworld.Data{Name: "World"}
	processInstance := helloworld.NewHelloWorldProcess(id, engine, start, input)

	// Start the process instance
	_, err := engine.SendCommand(dynamap.StartProcessInstanceCmd{Instance: processInstance})
	if err != nil {
		log.Fatal(err)
	}
}

```

The final layout should be as follows:

![example/layout.png](example/layout.png)

10. Run the program:

```shell
go run .
```

11. Output:

```shell
Hello World
```