package dynamap

import (
	"context"
	"strconv"
)

type IdGenerator interface {
	GenerateId(elementType string) string
}

type MemoryIdGenerator struct {
	idChan chan int
	id     int
}

func NewMemoryIdGenerator() *MemoryIdGenerator {
	return &MemoryIdGenerator{}
}

func (g *MemoryIdGenerator) Start(ctx context.Context) {
	g.idChan = make(chan int)
	go func(g *MemoryIdGenerator) {
		defer close(g.idChan)
		for {
			select {
			case <-ctx.Done():
				return
			case g.idChan <- g.id:
				g.id++
			}
		}
	}(g)
}

func (g *MemoryIdGenerator) GenerateId(_ string) string {
	return strconv.Itoa(<-g.idChan)
}
