package dynamap

type CmdError interface {
	Error() error
}

type StartProcessInstanceCmd struct {
	Instance *ProcessInstance
}

type StartProcessInstanceResp struct {
	Id  string
	Err error
}

func (s StartProcessInstanceResp) Error() error {
	return s.Err
}

type GetProcessInstancesCmd struct {
}

type GetProcessInstancesResp struct {
	Data   []ProcessInstanceState
	Errors []error
}

func NewGetProcessInstancesResp() *GetProcessInstancesResp {
	return &GetProcessInstancesResp{Data: make([]ProcessInstanceState, 0), Errors: make([]error, 0)}
}

type GetTasksCmd struct{}
type GetTasksResp struct {
	Data   []TaskState
	Errors []error
}

func NewGetTasksResp() *GetTasksResp {
	return &GetTasksResp{Data: make([]TaskState, 0), Errors: make([]error, 0)}
}

type CompleteUserTasksCmd struct {
	Tasks []CompleteUserTaskCmd
}

type CompleteTasksResp struct {
	TasksResp []CompleteUserTaskResp
	Errors    []error
}

func NewCompleteTasksResp() *CompleteTasksResp {
	return &CompleteTasksResp{Errors: make([]error, 0)}
}
