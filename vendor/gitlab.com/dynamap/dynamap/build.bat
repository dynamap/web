cd ./builder/cmd/strm
SET GOOS=windows
go build -o ../../bin/windows/strm.exe
SET GOOS=linux
go build -o ../../bin/linux/strm
SET GOOS=darwin
go build -o ../../bin/mac/strm
cd ../../..