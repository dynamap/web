package dynamap

// TODO: Handle errors in saving state to state store.

import (
	"context"
	"fmt"
	"log"
	"time"
)

const (
	ElementCreating    = "Creating"
	ElementCreated     = "Created"
	ElementActivating  = "Activating"
	ElementActivated   = "Activated"
	ElementCompleting  = "Completing"
	ElementCompleted   = "Completed"
	ElementTerminating = "Terminating"
	ElementTerminated  = "Terminated"
)

const (
	ProcessInstanceType            = "ProcessInstance"
	TokenType                      = "Token"
	SequenceFlowType               = "SequenceFlow"
	ExclusiveGatewayType           = "ExclusiveGateway"
	ParallelGatewayType            = "ParallelGateway"
	NoneStartEventType             = "NoneStartEvent"
	NoneIntermediateThrowEventType = "NoneIntermediateThrowEvent"
	NoneEndEventType               = "NoneEndEvent"
	TimerStartEventType            = "TimerStartEventType"
	UserTaskType                   = "UserTaskType"
	ScriptTaskType                 = "ScriptTaskType"
)

var (
	ErrDataUnmarshal = NewBPMNEngineError(nil, "")
)

// LifeCycleRunner runs the life cycle of an element through all required states for the element such as Creating to Completed.
type LifeCycleRunner interface {
	RunLifecycle(ctx context.Context, bpmnEngine *BPMNEngine, token *Token) error
}

// ElementState contains the primary information required to describe the state of any element.
type ElementState struct {
	ElementType       string // The type of element, e.g. SequenceFlow, NoneStartEvent, ParallelGateway, etc.
	Key               string // The unique name for an element in a process at design time.
	Id                string // The unique id created at run-time for an instance of the element.
	Status            string // The status of the element state, e.g. Completing, Completed, etc.
	ProcessInstanceId string // The id of the process instance it belongs to
	TokenId           string // The id of the token it belongs to
	Object            any    `json:"-"` // A reference to the underlying element if required for direct access, e.g. to enable custom storage.
	Store             bool   // Whether to store the element state
}

// ElementStateProvider is an interface that provides the state of an element.
type ElementStateProvider interface {
	GetElementState() ElementState
}

// StartEvent represents a BPMN none start event.
type StartEvent struct {
	ElementState
}

// NewNoneStartEvent creates a new none start event.
func NewNoneStartEvent(pid, tid, key, id string) *StartEvent {
	return &StartEvent{ElementState: ElementState{ProcessInstanceId: pid, TokenId: tid, Key: key, Id: id, ElementType: NoneStartEventType, Status: ElementActivated}}
}

// GetElementState returns the state of the StartEvent instance
func (e *StartEvent) GetElementState() ElementState {
	return e.ElementState
}

func (e *StartEvent) RefElement() any {
	return e
}

func (e *StartEvent) RunLifecycle(ctx context.Context, bpmnEngine *BPMNEngine, token *Token) error {
	e.Status = ElementCompleted
	return nil
}

type TimerStartEvent struct {
	ElementState
}

func NewTimerStartEvent(pid, tid, key, id string) *TimerStartEvent {
	return &TimerStartEvent{ElementState: ElementState{ProcessInstanceId: pid, TokenId: tid, Key: key, Id: id, ElementType: TimerStartEventType}}
}

func (e *TimerStartEvent) GetElementState() ElementState {
	return e.ElementState
}

func (e *TimerStartEvent) RefElement() any {
	return e
}

func (e *TimerStartEvent) RunLifecycle(ctx context.Context, bpmnEngine *BPMNEngine, token *Token) error {
	e.Status = ElementCompleted
	return nil
}

type SendTask struct {
	ElementState
}

func (s *SendTask) RunLifecycle(ctx context.Context, bpmnEngine *BPMNEngine, token *Token) error {
	s.Status = ElementCompleted
	if s.Store {
		bpmnEngine.WriteElement(s.ElementState)
	}
	return nil
}

type SequenceFlow struct {
	ElementState
}

func (s *SequenceFlow) GetElementState() ElementState {
	return s.ElementState
}

func NewSequenceFlow(pid, tid, key, id string) *SequenceFlow {
	return &SequenceFlow{ElementState{ProcessInstanceId: pid, TokenId: tid, Key: key, Id: id, ElementType: SequenceFlowType}}
}

func (s *SequenceFlow) RunLifecycle(ctx context.Context, bpmnEngine *BPMNEngine, token *Token) error {
	s.Status = ElementCompleted
	if s.Store {
		bpmnEngine.WriteElement(s.ElementState)
	}
	return nil
}

type ExclusiveGateway struct {
	ElementState
	Handler     func() ElementStateProvider
	NextElement ElementStateProvider
}

func NewExclusiveGateway(pid, tid, key, id string, handler func() ElementStateProvider) *ExclusiveGateway {
	return &ExclusiveGateway{ElementState: ElementState{ProcessInstanceId: pid, TokenId: tid, Key: key, Id: id, ElementType: ExclusiveGatewayType}, Handler: handler}
}

func (eg *ExclusiveGateway) GetElementState() ElementState {
	return eg.ElementState
}

func (eg *ExclusiveGateway) RefElement() any {
	return eg
}

func (eg *ExclusiveGateway) RunLifecycle(ctx context.Context, bpmnEngine *BPMNEngine, token *Token) error {
	eg.Completing(ctx, bpmnEngine, token)
	eg.Completed(ctx, bpmnEngine, token)
	return nil
}

func (eg *ExclusiveGateway) Completing(ctx context.Context, bpmnEngine *BPMNEngine, token *Token) {
	eg.Status = ElementCompleting
	if eg.Store {
		bpmnEngine.WriteElement(eg.ElementState)
	}
	eg.NextElement = eg.Handler()
}

func (eg *ExclusiveGateway) Completed(ctx context.Context, bpmnEngine *BPMNEngine, token *Token) {
	eg.Status = ElementCompleted
	if eg.Store {
		bpmnEngine.WriteElement(eg.ElementState)
	}
}

type ScriptTask struct {
	ElementState
	script func(ctx context.Context, bpmnEngine *BPMNEngine, token *Token, task *ScriptTask)
}

func NewScriptTask(pid, tid, key, id string, script func(ctx context.Context, bpmnEngine *BPMNEngine, token *Token, task *ScriptTask)) *ScriptTask {
	return &ScriptTask{ElementState: ElementState{ProcessInstanceId: pid, TokenId: tid, Key: key, Id: id, ElementType: ScriptTaskType}, script: script}
}

func (st *ScriptTask) GetElementState() ElementState {
	return st.ElementState
}

func (st *ScriptTask) RunLifecycle(ctx context.Context, bpmnEngine *BPMNEngine, token *Token) error {
	st.Completing(ctx, bpmnEngine, token)
	st.Completed(ctx, bpmnEngine, token)
	return nil
}

func (st *ScriptTask) Completing(ctx context.Context, bpmnEngine *BPMNEngine, token *Token) {
	st.Status = ElementCompleting
	if st.Store {
		bpmnEngine.WriteElement(st.ElementState)
	}
	st.script(ctx, bpmnEngine, token, st)
}

func (st *ScriptTask) Completed(ctx context.Context, bpmnEngine *BPMNEngine, token *Token) {
	st.Status = ElementCompleted
	if st.Store {
		bpmnEngine.WriteElement(st.ElementState)
	}
}

type NoneIntermediateThrowEvent struct {
	ElementState
}

func NewNoneIntermediateThrowEvent(pid, tid, key, id string) *NoneIntermediateThrowEvent {
	return &NoneIntermediateThrowEvent{ElementState: ElementState{ProcessInstanceId: pid, TokenId: tid, Key: key, Id: id, ElementType: NoneIntermediateThrowEventType}}
}

func (e *NoneIntermediateThrowEvent) GetElementState() ElementState {
	return e.ElementState
}

func (e *NoneIntermediateThrowEvent) RefElement() any {
	return e
}

func (e *NoneIntermediateThrowEvent) RunLifecycle(ctx context.Context, bpmnEngine *BPMNEngine, token *Token) error {
	e.Status = ElementCompleted
	if e.Store {
		bpmnEngine.WriteElement(e.ElementState)
	}
	return nil
}

type NoneEndEvent struct {
	ElementState
}

func NewNoneEndEvent(pid, tid, key, id string, store bool) *NoneEndEvent {
	return &NoneEndEvent{ElementState: ElementState{ProcessInstanceId: pid, TokenId: tid, Key: key, Id: id, ElementType: NoneEndEventType, Store: store}}
}

func (e *NoneEndEvent) GetElementState() ElementState {
	return e.ElementState
}

func (e *NoneEndEvent) RefElement() any {
	return e
}

func (e *NoneEndEvent) RunLifecycle(ctx context.Context, bpmnEngine *BPMNEngine, token *Token) error {
	e.Status = ElementCompleted
	if e.Store {
		bpmnEngine.WriteElement(e.ElementState)
	}
	return nil
}

type BaseTask struct {
	CompletedBy
	Comments
}

type CompletedBy struct {
	CompletedById   string
	CompletedByUser string
}

type Comments struct {
	Comments string
}

type UserTask[T any] struct {
	ElementState
	completed     bool
	CompletedTime time.Time
	Data          T
}

type GetUserTaskCmd struct {
}

type CompleteUserTaskCmd struct {
	ProcessInstanceId string
	TaskId            string
	Data              any
}

type CompleteUserTaskResp struct {
	ProcessInstanceId string
	TaskId            string
	Error             error
}

func NewUserTask[T any](pid, tid, key, id, status string, store bool) *UserTask[T] {
	task := UserTask[T]{ElementState: ElementState{ProcessInstanceId: pid, TokenId: tid, Key: key, Id: id, ElementType: UserTaskType, Status: status, Store: store}}
	task.Object = &task
	return &task
}

func (t *UserTask[T]) GetElementState() ElementState {
	return t.ElementState
}

func (t *UserTask[T]) RefElement() any {
	return t
}

func (t *UserTask[T]) RunLifecycle(ctx context.Context, bpmnEngine *BPMNEngine, token *Token) error {
	if err := t.Activated(ctx, bpmnEngine, token); err != nil {
		return err
	}
	t.Completed(ctx, bpmnEngine, token)
	return nil
}

func (t *UserTask[T]) Activated(ctx context.Context, bpmnEngine *BPMNEngine, token *Token) error {
	t.Status = ElementActivated
	if t.Store {
		bpmnEngine.WriteElement(t.ElementState)
	}
	return t.RunTask(ctx, bpmnEngine, token)
}

func (t *UserTask[T]) Completed(ctx context.Context, bpmnEngine *BPMNEngine, token *Token) {
	t.Status = ElementCompleted
	if t.Store {
		bpmnEngine.WriteElement(t.ElementState)
	}
}

func (t *UserTask[T]) RunTask(ctx context.Context, bpmnEngine *BPMNEngine, token *Token) error {
	token.setAcceptingCommands(true)
	defer func() {
		token.setAcceptingCommands(false)
	}()
	for {
		select {
		case <-ctx.Done():
			return fmt.Errorf("usertask %s cancelled", t.Key)
		case cmd := <-token.cmdReq:
			switch c := cmd.(type) {
			case CompleteUserTaskCmd:
				resp := CompleteUserTaskResp{token.ProcessInstance.Id, t.Id, nil}
				if c.TaskId == t.Id && c.ProcessInstanceId == token.ProcessInstance.Id {
					data, ok := c.Data.(T)
					if ok {
						// Task Complete
						t.completed = true
						t.CompletedTime = time.Now()
						t.Data = data
						token.cmdResp <- resp
						return nil
					} else {
						err := fmt.Errorf("unable to marshal data to type %T", t.Data)
						resp.Error = NewBPMNEngineError(err, "completeUserTaskCmd error")
						log.Println(resp.Error)
					}
				} else {
					err := fmt.Errorf("got processId %s and taskId %s, want processId %s, taskId %s", c.ProcessInstanceId, c.TaskId, token.ProcessInstance.Id, t.Id)
					resp.Error = NewBPMNEngineError(err, "completeUserTaskCmd error")
				}
				token.cmdResp <- resp
			case GetUserTaskCmd:
				pi := token.ProcessInstance
				token.cmdResp <- TaskState{
					ProcessInstanceKey:     pi.Key,
					ProcessInstanceId:      pi.Id,
					ProcessInstanceVersion: pi.Version,
					Key:                    t.Key,
					Id:                     t.Id,
					Status:                 t.Status,
					Data:                   t.Data, // Todo: Copy data
				}
			}
		}
	}
}

func (t *UserTask[T]) Clone() *UserTask[T] {
	newTask := UserTask[T]{ElementState: ElementState{
		Key:         t.Key,
		Id:          t.Id,
		ElementType: t.ElementType,
		Status:      t.Status,
		//Created:     t.Created, //TODO: ADD
	},
		completed:     t.completed,
		CompletedTime: t.CompletedTime,
	}

	return &newTask
}

type ParallelGatewayCmd struct {
	pg ParallelGateway
}

type ParallelGateway struct {
	ElementState
	CurrentSeqKey string
	InSeqKeys     []string
	OutSeqKeys    []string
}

func NewParallelGateway(pid, tid, key, id, currentSeqKey string, inSeqKeys, outSeqKeys []string) *ParallelGateway {
	return &ParallelGateway{
		ElementState:  ElementState{ProcessInstanceId: pid, TokenId: tid, Key: key, Id: id, ElementType: ParallelGatewayType},
		CurrentSeqKey: currentSeqKey,
		InSeqKeys:     inSeqKeys,
		OutSeqKeys:    outSeqKeys,
	}
}

func (pg *ParallelGateway) GetElementState() ElementState {
	return pg.ElementState
}

func (pg *ParallelGateway) RefElement() any {
	return pg
}

func (pg *ParallelGateway) RunLifecycle(ctx context.Context, bpmnEngine *BPMNEngine, token *Token) error {
	if err := pg.Completing(ctx, bpmnEngine, token); err != nil {
		return err
	}
	pg.Completed(ctx, bpmnEngine, token)
	return nil
}

func (pg *ParallelGateway) Completing(ctx context.Context, bpmnEngine *BPMNEngine, token *Token) error {
	pg.Status = ElementCompleting
	if pg.Store {
		bpmnEngine.WriteElement(pg.ElementState)
	}
	for {
		select {
		case <-ctx.Done():
			return fmt.Errorf("parallel gateway %s: cancelling", pg.Key)
		default:
			resp, err := token.ProcessInstance.SendCommand(*pg)
			if err != nil {
				fmt.Printf("parallel gateway %s: %v\n", pg.Key, err)
				time.Sleep(1 * time.Millisecond)
				continue
			}
			done, ok := resp.(<-chan any)
			if !ok {
				fmt.Printf("parallel gateway: completing received: %v", done)
				time.Sleep(1 * time.Millisecond)
				continue
			}
			<-done
			return nil
		}
	}
	return nil
}

func (pg *ParallelGateway) Completed(ctx context.Context, bpmnEngine *BPMNEngine, token *Token) {
	pg.Status = ElementCompleted
	if pg.Store {
		bpmnEngine.WriteElement(pg.ElementState)
	}
}
