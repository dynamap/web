package dynamap

type ProcessInstanceCmd struct {
	Id  string
	Cmd any
}
type ExecuteCmd interface {
	ExecuteCmd(cmd any) any
}
type GetTasksInfoCmd struct{}
type GetProcessInstanceInfoCmd struct{}
type PingProcessInstanceCmd struct{}
type processCompletedCmd struct {
	Id string
}
