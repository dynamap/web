package dynamap

import (
	"context"
	"errors"
	"fmt"
	"log"
	"reflect"
	"strconv"
	"sync"
	"sync/atomic"
	"time"
)

var (
	ErrProcessStateMachineRequired  = NewBPMNEngineError(nil, "process: process instance implementation must implement ProcessStateMachine")
	ErrProcessInstanceNotRunning    = NewBPMNEngineError(nil, "process: process instance is completed")
	ErrProcessSendCommandTimeout    = NewBPMNEngineError(nil, "process: send command timeout.")
	ErrProcessReceiveCommandTimeout = NewBPMNEngineError(nil, "process: receive command timeout.")
)

type ProcessStateMachine interface {
	GetNextElement(engine *BPMNEngine, g IdGenerator, t *Token, currentElement ElementStateProvider) ElementStateProvider
}

type ProcessInstanceDataRetriever interface {
	ProcessInstanceData() any
}

type ProcessInstanceState struct {
	Key     string
	Id      string
	Version string
	Status  string
	Created time.Time
	Data    any
}

type TaskState struct {
	ProcessInstanceKey     string
	ProcessInstanceId      string
	ProcessInstanceVersion string
	Key                    string
	Id                     string
	Status                 string
	Data                   any
}

type ProcessInstance struct {
	Mu                      *sync.RWMutex
	Key                     string
	Id                      string
	Version                 string
	Status                  string
	Created                 time.Time
	StartElement            ElementStateProvider
	Tokens                  []*Token
	BpmnEngine              *BPMNEngine
	Impl                    any // Implementation of BPMN Process
	psm                     ProcessStateMachine
	ctx                     context.Context
	cancel                  context.CancelFunc
	twg                     *sync.WaitGroup
	waitingParallelGateways map[string]*WaitingParallelGateway
	cmdReq                  chan any
	cmdResp                 <-chan any
	done                    chan any
	running                 uint32
}

func (p *ProcessInstance) String() string {
	return p.Id + ": " + strconv.Itoa(len(p.Tokens)) + " Tokens"
}

func (p *ProcessInstance) RemoveToken(token *Token) {
	ti := -1
	for i := range p.Tokens {
		t := p.Tokens[i]
		if t == token {
			ti = i
			break
		}
	}
	if ti == -1 {
		return
	}
	p.Tokens[ti] = p.Tokens[len(p.Tokens)-1]
	p.Tokens = p.Tokens[:len(p.Tokens)-1]
}

func (p *ProcessInstance) RefElement() any {
	return p
}

func (p *ProcessInstance) Run(wg *sync.WaitGroup) error {
	// Note These Two Each Start a Go Routine
	// Should Not Access Mutable Data Without Synchronization
	// Todo: Could we combine these into one go routine?
	<-p.createCommandProcessor()
	<-p.runStateMachine(wg)
	return nil
}

func (p *ProcessInstance) InitProcessInstance(ctx context.Context, cancel context.CancelFunc) error {
	p.ctx = ctx // Needed for sending commands
	p.cancel = cancel
	p.Mu = &sync.RWMutex{}
	p.done = make(chan any)
	p.waitingParallelGateways = make(map[string]*WaitingParallelGateway)
	psm, ok := p.Impl.(ProcessStateMachine)
	if !ok {
		return ErrProcessStateMachineRequired
	}
	p.psm = psm

	if p.Status == ElementCreating {
		err := p.Creating()
		if err != nil {
			return err
		}
		p.Status = ElementCreated
	}

	if p.Tokens != nil && len(p.Tokens) > 0 {
		return nil
	}

	// Create Token
	token := NewToken(p.StartElement, p, p.BpmnEngine)
	// This is a hack to set the token id. When a process instance is created with a start element, the token doesn't
	// exist yet, so the token id is not set. There isn't a mechanism to set the token id with a TokenStateProvider
	{
		s := reflect.ValueOf(p.StartElement)
		tid := p.StartElement.GetElementState().TokenId
		if tid != "" {
			log.Printf("process instance run: warning start element '%+v' has TokenId set, should be blank", p.StartElement)
		}
		reflect.Indirect(s).FieldByName("TokenId").SetString(token.Id)
		tid = p.StartElement.GetElementState().TokenId
		if tid != token.Id {
			log.Printf("process instance run: token id should be %s, got %s", token.Id, tid)
		}
	}
	p.Tokens = []*Token{token}
	return nil
}

// Todo: RunToCompletion missing some initialization done in Run()
func (p *ProcessInstance) RunToCompletion(ctx context.Context) {
	t := p.Tokens[0]
	for t.CurrentElement != nil {
		t.CurrentElement = p.psm.GetNextElement(p.BpmnEngine, p.BpmnEngine.IdGenerator, t, t.CurrentElement)
		if t.CurrentElement == nil {
			break
		}
		currentLifeCycleRunner, ok := t.CurrentElement.(LifeCycleRunner)
		if !ok {
			log.Printf("process instance run: cannot convert element %+v to LifeCycleRunner", currentLifeCycleRunner)
			break
		}
		currentLifeCycleRunner.RunLifecycle(ctx, t.BpmnEngine, t)
	}
}

func (p *ProcessInstance) setRunning(running bool) {
	if running {
		atomic.StoreUint32(&p.running, 1)
	} else {
		atomic.StoreUint32(&p.running, 0)
	}
}

func (p *ProcessInstance) Running() bool {
	return atomic.LoadUint32(&p.running) == 1
}

func (p *ProcessInstance) createCommandProcessor() <-chan struct{} {
	ready := make(chan struct{})
	p.cmdReq = make(chan any)

	// Create Command Go Routine
	cmdFunc := func(done <-chan any, cancel <-chan struct{}, reqChan <-chan any) <-chan any {
		respCh := make(chan any)
		go func() {
			defer func() {
				p.setRunning(false)
			}()
			p.setRunning(true)
			ready <- struct{}{}
			for {
				select {
				case <-cancel:
					return
				case <-done:
					return
				case cmd := <-reqChan:
					switch v := cmd.(type) {
					case PingProcessInstanceCmd:
						respCh <- v // Return original cmd as an 'ack'
					case GetProcessInstanceInfoCmd:
						respCh <- p.GetProcessInstanceInfo()
					case GetTasksInfoCmd:
						respCh <- p.GetTasksInfo()
					case CompleteUserTaskCmd:
						respCh <- p.CompleteTask(v)
					case ParallelGateway:
						respCh <- p.ProcessParallelGateway(v)
					default:
						execCmd, ok := p.Impl.(ExecuteCmd)
						if ok {
							respCh <- execCmd.ExecuteCmd(cmd)
						} else {
							respCh <- v // Todo: Implement commands
						}
					}
				}
			}
		}()
		return respCh
	}
	p.cmdResp = cmdFunc(p.done, p.ctx.Done(), p.cmdReq)
	return ready
}

func (p *ProcessInstance) runStateMachine(wg *sync.WaitGroup) <-chan struct{} {
	ready := make(chan struct{})

	go func() {
		defer wg.Done()
		ready <- struct{}{}
		for {
			switch p.Status {
			case ElementCreated:
				p.Activating()
				p.setStatus(ElementActivating)
			case ElementActivating:
				p.Activated()
				p.setStatus(ElementActivated)
			case ElementActivated:
				p.Completing()
				for i := range p.Tokens {
					if !p.Tokens[i].complete {
						// Tokens still active due to shutdown.
						p.BpmnEngine.deleteProcessInstance(p.Id)
						return
					}
				}
				p.setStatus(ElementCompleting)
			case ElementCompleting:
				p.setStatus(ElementCompleted)
				p.BpmnEngine.deleteProcessInstance(p.Id)
				p.Completed()
			case ElementCompleted:
				close(p.done)
				return
			}
			p.WriteState()
		}
	}()
	return ready
}

// SendCommand Called from external client to communicate with process instance
func (p *ProcessInstance) SendCommand(cmd any) (any, error) {
	resp, err := p.SendCommandWithTimeout(cmd, 30*time.Second)
	if err != nil {
		log.Printf("send cmd: %v", err)
	}
	return resp, err
}

func (p *ProcessInstance) SendCommandWithTimeout(cmd any, timeout time.Duration) (any, error) {
	if !p.Running() {
		return nil, ErrProcessInstanceNotRunning // No channel/go routine listening for commands.
	}
	select {
	case <-p.ctx.Done():
		return nil, errors.New("process: cancelled") // Todo: Return an error
	case p.cmdReq <- cmd:
		return <-p.cmdResp, nil
	case <-time.After(timeout):
		err := ErrProcessSendCommandTimeout
		err.Message += fmt.Sprintf(" %#v", cmd)
		return nil, err
	}
}

func (p *ProcessInstance) ping() error {
	cmd := PingProcessInstanceCmd{}
	select {
	case <-p.ctx.Done():
		return errors.New("process: cancelled")
	case p.cmdReq <- cmd:
		<-p.cmdResp
		return nil
	case <-time.After(1 * time.Second):
		err := ErrProcessSendCommandTimeout
		err.Message += fmt.Sprintf(" %#v", cmd)
		return err
	}
	return nil
}

func (p *ProcessInstance) WriteState() {
	state := p.ProcessInstanceState()
	p.BpmnEngine.WriteProcessInstanceState(state)
}

func (p *ProcessInstance) ProcessInstanceState() ProcessInstanceState {
	p.Mu.RLock()
	state := ProcessInstanceState{
		Key:    p.Key,
		Id:     p.Id,
		Status: p.Status,
	}
	p.Mu.RUnlock()
	if data, ok := p.Impl.(ProcessInstanceDataRetriever); ok {
		state.Data = data.ProcessInstanceData()
	}
	return state
}

func (p *ProcessInstance) Creating() error {
	state := p.ProcessInstanceState()
	id := ""
	var err error
	id, err = p.BpmnEngine.StateStore.CreateProcessInstance(state)
	if err != nil {
		return err
	}

	// Store returned id
	p.Id = id
	return nil
}

func (p *ProcessInstance) Activating() {
}

func (p *ProcessInstance) Activated() {
}

func (p *ProcessInstance) Completing() {
	// Token Wait Group
	p.twg = &sync.WaitGroup{}
	p.twg.Add(len(p.Tokens))

	// Run Tokens
	for i := range p.Tokens {
		go p.Tokens[i].Run(p.ctx, p.twg)
	}
	p.twg.Wait()
}

func (p *ProcessInstance) Completed() {
}

func (p *ProcessInstance) Terminating() {
}

func (p *ProcessInstance) Terminated() {
}

func (p *ProcessInstance) Shutdown() {
	if p.cancel != nil {
		p.cancel()
	}
}

func (p *ProcessInstance) setStatus(status string) {
	p.Mu.Lock()
	defer p.Mu.Unlock()
	p.Status = status
}

func (p *ProcessInstance) GetProcessInstanceInfo() ProcessInstanceState {
	p.Mu.RLock()
	defer p.Mu.RUnlock()
	return ProcessInstanceState{
		Key:     p.Key,
		Id:      p.Id,
		Status:  p.Status,
		Created: p.Created,
	}
}

func (p *ProcessInstance) GetTasksInfo() []TaskState {
	tokens := copyTokenSlice(p)
	taskInfos := make([]TaskState, 0, len(tokens))
	for i := range tokens {
		t := tokens[i]
		if t.IsComplete() {
			continue
		}
		resp := t.SendCommand(p.ctx, GetUserTaskCmd{}, 1*time.Second)
		if resp == nil || resp == "" {
			continue
		}
		tInfo, ok := resp.(TaskState)
		if !ok {
		}
		taskInfos = append(taskInfos, tInfo)
	}
	return taskInfos
}

func (p *ProcessInstance) CompleteTask(cmd CompleteUserTaskCmd) any {
	resp := NewCompleteTasksResp()
	wg := sync.WaitGroup{}
	tokens := copyTokenSlice(p)
	wg.Add(len(tokens))
	ch := make(chan CompleteUserTaskResp)
	go func() {
		wg.Wait()
		close(ch)
	}()
	// Send complete task command to all process instance tokens, since we don't know which one has the task.
	for i := range tokens {
		t := tokens[i]
		go func(t *Token, ch chan<- CompleteUserTaskResp) {
			defer wg.Done()
			tResp := t.SendCommand(p.ctx, cmd, 1*time.Millisecond)
			if tResp != nil {
				ch <- tResp.(CompleteUserTaskResp)
			}
			ch <- CompleteUserTaskResp{Error: errors.New("process instance complete task: nil token resp")}
		}(t, ch)
	}
	for tResp := range ch {
		// Responses can come from tokens that don't have the task. Only add responses for the matching task.
		if tResp.ProcessInstanceId == cmd.ProcessInstanceId && tResp.TaskId == cmd.TaskId {
			resp.TasksResp = append(resp.TasksResp, tResp)
		}
	}
	return *resp
}

type WaitingParallelGateway struct {
	key   string
	count int
	done  chan any
}

func (p *ProcessInstance) ProcessParallelGateway(v ParallelGateway) <-chan any {
	wpg, ok := p.waitingParallelGateways[v.Key]
	if !ok {
		done := make(chan any)
		wpg = &WaitingParallelGateway{key: v.Key, count: len(v.InSeqKeys), done: done}
		p.waitingParallelGateways[v.Key] = wpg
	}
	wpg.count--
	if wpg.count == 0 {
		// Lock
		p.Mu.Lock()
		defer p.Mu.Unlock()

		// Create Tokens
		for _, outKey := range v.OutSeqKeys {
			id := (*p.BpmnEngine).GenerateId(SequenceFlowType)
			token := NewToken(NewSequenceFlow(p.Id, "", outKey, id), p, p.BpmnEngine)
			token.CurrentElement.(*SequenceFlow).TokenId = token.Id
			p.Tokens = append(p.Tokens, token)
			p.twg.Add(1)
			go token.Run(p.ctx, p.twg)
		}

		// Todo: Delete merged tokens

		// Signal Waiting Tokens to End
		close(wpg.done)
	} else if wpg.count < 0 {
		fmt.Printf("processParallelGateway error: negative count: %d\n", wpg.count)
	}
	return wpg.done
}

// copyTokenSlice creates a new slice containing the current process tokens.
func copyTokenSlice(p *ProcessInstance) []*Token {
	p.Mu.RLock()
	defer p.Mu.RUnlock()
	newTokenSlice := make([]*Token, len(p.Tokens))
	for i := range p.Tokens {
		newTokenSlice[i] = p.Tokens[i]
	}
	return newTokenSlice
}
