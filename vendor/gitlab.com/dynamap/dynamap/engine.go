package dynamap

// Todo: Need to be able to create a new process, token, element when the Id can't be generated in advance.
// Need to be able to return the id back and update it in memory in the engine before proceeding in the lifecycle.

import (
	"context"
	"errors"
	"fmt"
	"runtime/debug"
	"sync"
	"time"
)

var (
	ErrBPMNEngineNotStarted                 = NewBPMNEngineError(nil, "engine: not started.")
	ErrBPMNEngineCancelled                  = NewBPMNEngineError(nil, "engine: cancelled.")
	ErrBPMNEngineIdGeneratorNotSet          = NewBPMNEngineError(nil, "engine: Id Generator not set.")
	ErrBPMNEngineProcessInstanceStoreNotSet = NewBPMNEngineError(nil, "engine: Process Instance Store not set.")
	ErrBPMNEngineTokenStoreNotSet           = NewBPMNEngineError(nil, "engine: Token Store not set.")
	ErrBPMNEngineStateStoreNotSet           = NewBPMNEngineError(nil, "engine: State Store not set.")
	ErrBPMNEngineSendCommandTimeout         = NewBPMNEngineError(nil, "engine: send command timeout.")
	ErrBPMNEngineReceiveCommandTimeout      = NewBPMNEngineError(nil, "engine: receive command timeout.")
	ErrBPMNEngineProcessInstanceNotFound    = NewBPMNEngineError(nil, "engine: process instance not found.")
	ErrBPMNEngineProcessInstanceNotRunning  = NewBPMNEngineError(nil, "engine: process instance not running.")
)

// A StartEventProcessor is responsible for starting processes.
type StartEventProcessor interface {
	StartEventProcesses(ctx context.Context)
}

// BPMNEngine manages starting, stopping, and sending commands to process instances.
type BPMNEngine struct {
	IdGenerator          IdGenerator                 // Generates ids for elements
	StartEventProcessors []StartEventProcessor       // Start Event Processors to run on startup
	ProcessInstanceStore ProcessInstanceStore        // Stores process instance states received from state stream
	TokenStore           TokenStore                  // Stores token states received from state stream
	StateStore           StateStore                  // Stores states received from stateStream
	Ctx                  context.Context             // Context for the workload engine
	cancel               context.CancelFunc          // Used to cancel running the workload engine
	cmdReq               chan any                    // Commands to the engine are sent on the request channel
	cmdResp              <-chan any                  // Responses to commands are sent on the response channel
	wgProcessInstance    sync.WaitGroup              // Wait group to wait for process instance goroutines to finish
	wgStateStream        sync.WaitGroup              // Wait group to wait for the state stream goroutine to finish
	stateStream          chan any                    // Channel for state changes
	processInstances     map[string]*ProcessInstance // Maps a process instance Id to the process instance. Must be a pointer to prevent copying of the process instance.
	mu                   sync.RWMutex
}

// NewBPMNEngine creates a new BPMNEngine with given IdGenerator, StateStore
func NewBPMNEngine(g IdGenerator, pis ProcessInstanceStore, ts TokenStore, ss StateStore) *BPMNEngine {
	ctx, cancel := context.WithCancel(context.Background())
	b := BPMNEngine{
		IdGenerator:          g,
		StartEventProcessors: make([]StartEventProcessor, 0),
		ProcessInstanceStore: pis,
		TokenStore:           ts,
		StateStore:           ss,
		Ctx:                  ctx,
		cancel:               cancel,
		processInstances:     make(map[string]*ProcessInstance),
		stateStream:          make(chan any),
		wgProcessInstance:    sync.WaitGroup{},
		wgStateStream:        sync.WaitGroup{},
	}
	return &b
}

// NewMemoryBPMNEngine creates a new BPMNEngine with an in-memory id generator, state store, and history store.
func NewMemoryBPMNEngine() *BPMNEngine {
	g := NewMemoryIdGenerator()
	s := NewMemoryStateStore()
	b := NewBPMNEngine(g, s, s, s)
	g.Start(b.Ctx)
	return b
}

// AddStartEventProcessor  adds an StartEventProcessor to the engine.
func (b *BPMNEngine) AddStartEventProcesser(processor StartEventProcessor) {
	b.StartEventProcessors = append(b.StartEventProcessors, processor)
}

// Start starts the engine, and starts go routines for the state strearm, commands, and starteventprocessors.
func (b *BPMNEngine) Start() error {
	if b.IdGenerator == nil {
		return ErrBPMNEngineIdGeneratorNotSet
	}

	if b.ProcessInstanceStore == nil {
		return ErrBPMNEngineProcessInstanceStoreNotSet
	}

	if b.TokenStore == nil {
		return ErrBPMNEngineTokenStoreNotSet
	}

	if b.StateStore == nil {
		return ErrBPMNEngineStateStoreNotSet
	}

	b.startCommandRoutine()
	b.startStartEventProcessors()

	return nil
}

// RunProcessInstanceToCompletion starts a new process instance and runs it to completion synchronously.
func (b *BPMNEngine) RunProcessInstanceToCompletion(pi *ProcessInstance) error {
	ctx, cancel := context.WithCancel(b.Ctx)
	err := pi.InitProcessInstance(ctx, cancel)
	if err != nil {
		return err
	}
	b.addProcessInstance(pi)
	pi.RunToCompletion(b.Ctx)
	b.deleteProcessInstance(pi.Id)
	return nil
}

// startCommandRoutine starts the command processing go routine.
func (b *BPMNEngine) startCommandRoutine() {
	cmdFunc := func(done <-chan struct{}, reqChan <-chan any) <-chan any {
		respCh := make(chan any)
		go func() {
			defer func() {
				close(respCh)
			}()
			for {
				select {
				case <-done:
					return
				case data := <-reqChan:
					switch v := data.(type) {
					case StartProcessInstanceCmd:
						id, err := b.startProcessInstanceAsync(v.Instance)
						respCh <- StartProcessInstanceResp{Id: id, Err: err}
					case GetProcessInstancesCmd:
						respCh <- *b.getProcessInstances()
					case GetTasksCmd:
						respCh <- *b.getTasks()
					case CompleteUserTasksCmd:
						respCh <- *b.completeTasks(v)
					case ProcessInstanceCmd:
						resp, err := b.sendProcessInstanceCmd(v)
						if err != nil {
							respCh <- err
						}
						respCh <- resp
					case processCompletedCmd:
						b.deleteProcessInstance(v.Id)
						respCh <- v
					default:
						respCh <- v // Todo: Implement commands
					}
				}
			}
		}()
		return respCh
	}

	b.cmdReq = make(chan any)
	b.cmdResp = cmdFunc(b.Ctx.Done(), b.cmdReq)
}

// addProcessInstance adds a process instance to the engine process instance map
func (b *BPMNEngine) addProcessInstance(pi *ProcessInstance) {
	b.mu.Lock()
	b.processInstances[pi.Id] = pi
	b.mu.Unlock()
}

// deleteProcessInstance deletes a process instance from the engine process instance map
func (b *BPMNEngine) deleteProcessInstance(id string) {
	b.mu.Lock()
	delete(b.processInstances, id)
	b.mu.Unlock()
}

// SendCommand sends any command to a process instance
func (b *BPMNEngine) SendCommand(cmd any) (any, error) {
	resp, err := b.SendCommandWithTimeout(cmd, 1000*time.Millisecond)
	if err != nil {
		return resp, err
	}
	if cmdErr, ok := resp.(CmdError); ok {
		return resp, cmdErr.Error()
	}
	return resp, err
}

// SendCommandWithTimeout sends any command to a process instance with a timeout
func (b *BPMNEngine) SendCommandWithTimeout(cmd any, timeout time.Duration) (any, error) {
	if b.Ctx == nil || b.cmdReq == nil || b.cmdResp == nil {
		return nil, ErrBPMNEngineNotStarted
	}
	select {
	case <-b.Ctx.Done():
		err := ErrBPMNEngineCancelled
		err.Message += fmt.Sprintf(" \n\t%#v", cmd)
		return nil, err
	case b.cmdReq <- cmd:
		return <-b.cmdResp, nil
	case <-time.After(timeout):
		err := ErrBPMNEngineSendCommandTimeout
		err.Message += fmt.Sprintf(" %#v", cmd)
		return nil, err
	}
}

// Wait waits for all process instances to complete
func (b *BPMNEngine) Wait() {
	b.wgProcessInstance.Wait()
	lastCount := 0
	count := 0
	identicalCount := 0
	for {
		lastCount = count
		count = b.ProcessInstanceCount()
		if count > 0 {
			time.Sleep(1 * time.Microsecond)
			if lastCount == count {
				identicalCount += 1
			}
			if identicalCount == 10000 {
				fmt.Println("identical count... stuck?")
			}
		} else {
			return
		}
	}
}

// Stop stops the engine
func (b *BPMNEngine) Stop() {
	close(b.stateStream)
	b.wgStateStream.Wait()
	b.cancel()
}

// WriteProcessInstanceState sends the process instance state to the state stream for writing
func (b *BPMNEngine) WriteProcessInstanceState(state ProcessInstanceState) error {
	return b.ProcessInstanceStore.WriteProcessInstanceState(state)
}

// WriteToken sends the token state to the state stream for writing
func (b *BPMNEngine) WriteToken(state TokenState) error {
	return b.TokenStore.WriteToken(state)
}

// WriteState sends the element state to the state stream for writing
func (b *BPMNEngine) WriteElement(state ElementState) error {
	return b.StateStore.WriteElement(state)
}

// GenerateId generates a new id for a given element type
func (b *BPMNEngine) GenerateId(elementType string) string {
	return b.IdGenerator.GenerateId(elementType)
}

// startProcessInstanceAsync starts a process instance in a new go routine and returns immediately
// returns the id of the new process instance which may have been automatically generated by the store.
// returns an error if the state could not be written to the store or an error starting the process instance.
func (b *BPMNEngine) startProcessInstanceAsync(pi *ProcessInstance) (string, error) {
	ctx, cancel := context.WithCancel(b.Ctx)
	err := pi.InitProcessInstance(ctx, cancel)
	if err != nil {
		return pi.Id, err
	}

	// Increase wait group for new go routine for process pi
	b.wgProcessInstance.Add(1)

	// Run the process pi. Starts a go routine
	err = pi.Run(&b.wgProcessInstance)
	if err != nil {
		return pi.Id, err
	}

	// Add the process pi for future reference
	// Must be called after run, otherwise the process pi could receive a command before the context is set.
	b.addProcessInstance(pi)
	return pi.Id, nil
}

// getProcessInstances returns information on all running process instances
func (b *BPMNEngine) getProcessInstances() *GetProcessInstancesResp {
	resp := NewGetProcessInstancesResp()
	var pis []*ProcessInstance
	b.mu.RLock()
	{
		pis = make([]*ProcessInstance, 0, len(b.processInstances))
		for i := range b.processInstances {
			pis = append(pis, b.processInstances[i])
		}
	}
	b.mu.RUnlock()

	for i := range pis {
		p := pis[i]
		if !p.Running() {
			resp.Errors = append(resp.Errors, BPMNEngineError{Inner: errors.New("engine: process not running")})
			continue
		}
		pResp, err := p.SendCommand(GetProcessInstanceInfoCmd{})
		if err != nil {
			resp.Errors = append(resp.Errors, BPMNEngineError{Inner: err})
			continue
		}
		pInfo, ok := pResp.(ProcessInstanceState)
		if !ok {
			resp.Errors = append(resp.Errors, BPMNEngineError{Inner: errors.New("engine: GetProcessInstanceInfoCmd response is not type ProcessInstanceState")})
			continue
		}
		resp.Data = append(resp.Data, pInfo)
	}
	return resp
}

// getTasks returns information on all running tasks
func (b *BPMNEngine) getTasks() *GetTasksResp {
	resp := NewGetTasksResp()
	b.mu.RLock()
	var pis []*ProcessInstance
	{
		pis = make([]*ProcessInstance, 0, len(b.processInstances))
		for i := range b.processInstances {
			pis = append(pis, b.processInstances[i])
		}
	}
	b.mu.RUnlock()
	for i := range pis {
		p := pis[i]
		if !p.Running() {
			continue
		}
		tResp, err := p.SendCommand(GetTasksInfoCmd{})
		if err != nil {
			resp.Errors = append(resp.Errors, err)
			continue
		}
		tInfos, ok := tResp.([]TaskState)
		if !ok {
			continue
		}
		for _, tInfo := range tInfos {
			resp.Data = append(resp.Data, tInfo)
		}
	}
	return resp
}

// completeTasks completes the requested tasks
func (b *BPMNEngine) completeTasks(v CompleteUserTasksCmd) *CompleteTasksResp {
	resp := NewCompleteTasksResp()
	for _, tc := range v.Tasks {
		b.mu.RLock()
		p, ok := b.processInstances[tc.ProcessInstanceId]
		b.mu.RUnlock()
		if !ok {
			resp.Errors = append(resp.Errors, ErrBPMNEngineProcessInstanceNotFound)
			continue
		}
		if !p.Running() {
			resp.Errors = append(resp.Errors, ErrBPMNEngineProcessInstanceNotRunning)
			continue
		}
		tResp, err := p.SendCommand(tc)
		if err != nil {
			resp.Errors = append(resp.Errors, err)
		}
		if tResp != nil {
			tasks := tResp.(CompleteTasksResp)
			for _, t := range tasks.TasksResp {
				resp.TasksResp = append(resp.TasksResp, t)
			}
			for _, e := range tasks.Errors {
				resp.Errors = append(resp.Errors, e)
			}
		}
	}
	return resp
}

// sendProcessInstanceCmd sends a command to a specific process instance
func (b *BPMNEngine) sendProcessInstanceCmd(cmd ProcessInstanceCmd) (any, error) {
	b.mu.RLock()
	p, ok := b.processInstances[cmd.Id]
	b.mu.RUnlock()
	if !ok {
		fmt.Println("Could not find process instance with id: ", cmd.Id)
		return nil, ErrBPMNEngineProcessInstanceNotFound
	}
	if !p.Running() {
		return nil, ErrBPMNEngineProcessInstanceNotRunning
	}
	return p.SendCommand(cmd.Cmd)
}

// ProcessInstanceCount returns the count of process instances.
func (b *BPMNEngine) ProcessInstanceCount() int {
	b.mu.RLock()
	defer b.mu.RUnlock()
	return len(b.processInstances)
}

// startStartEventProcessors starts execution of the startEventProcessors.
func (b *BPMNEngine) startStartEventProcessors() {
	for i := range b.StartEventProcessors {
		b.StartEventProcessors[i].StartEventProcesses(b.Ctx)
	}
}

// BPMNEngineError describes an error produced in the engine.
type BPMNEngineError struct {
	Inner      error          // Optional wrapped inner error
	Message    string         // Message describing the error
	StackTrace string         // Stacktrace in which the error was produced
	Misc       map[string]any // Optional information
}

// NewBPMNEngineError creates a new error related to the BPMNEngine.
func NewBPMNEngineError(err error, messagef string, msgArgs ...any) BPMNEngineError {
	return BPMNEngineError{
		Inner:      err,
		Message:    fmt.Sprintf(messagef, msgArgs...),
		StackTrace: string(debug.Stack()),
		Misc:       make(map[string]any),
	}
}

// Error implements the error interface and prints the BPMNEngineError message, inner error and stack trace.
func (err BPMNEngineError) Error() string {
	return fmt.Sprintf("%s: %s\n%s\n%+v", err.Message, err.Inner, err.StackTrace, err.Misc)
}
