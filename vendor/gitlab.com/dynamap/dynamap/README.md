# DynaMap™

DynaMap™ is a lightweight workflow engine written in Go for building business processes based on Business Process Model and
Notation 2.0 (BPMN).

The core of DynaMap is agnostic to how processes are started, created, or interacted with, making it suitable for
use in command line tools, microservices, web applications, or embedding with software written in another language.
Processes are compiled to standalone executables enabling deployment in a wide variety of environments.

Workflow engines are typically designed to store process state in a specific technology such as an SQL database.
This is an appealing approach initially, but it can have deployment, maintenance and performance limitations in the
long-run.

DynaMap core does not manage storing state externally, enabling customization by developers to suit their
requirements.
This enables tradeoffs in performance. For example in some cases, persisting process state is not required, but high
throughput and low latency is desired. In other cases, the ability to recover from a system crash and resume the process
is more important
than lower processing speeds due to serialization. DynaMap lets developers and businesses make these tradeoffs on a
process level, where different processes can have different storage requirements, instead of the entire engine forcing
every process to use the same storage mechanism and schema. This fits well with the database per microservice approach.
However, DynaMap can work in monolith, microservice, or serverless approaches.

## Repositories
The primary repository is located at [https://gitlab.com/dynamap](https://gitlab.com/dynamap). There are several
sub repositories:
- DynaMap Core: [https://gitlab.com/dynamap/dynamap](https://gitlab.com/dynamap/dynamap)
- DynaMap Web: [https://gitlab.com/dynamap/web](https://gitlab.com/dynamap/web)
- DynaMap Examples: [https://gitlab.com/dynamap/web](https://gitlab.com/dynamap/web)

## Dependencies
DynaMap core is designed to have no 3rd party dependencies other than Golang to keep the engine lightweight, and not
require managing 3rd party dependencies. Other layers on top of the core engine may contain dependencies, such as
DynaMap web services, data stores, and examples.

- Golang 1.20: [https://go.dev](https://go.dev)

## Getting started
The quickest way to get started, is with the [Hello World BPMN Process](HelloWorld.md) example.

Workflows may be created in open source and commercial BPMN modeling tools. For recommendations please reach out to
Quantek Systems.

The DynaMap Examples repository is the primary location for examples of different use cases:
[https://gitlab.com/dynamap/examples](https://gitlab.com/dynamap/examples)

Web services for processes may be generated using DynaMap Web: [https://gitlab.com/dynamap/web](https://gitlab.com/dynamap/web)

## Supported BPMN Elements

The following BPMN elements are currently implemented:

- None Start Event
- Timer Start Event (only repeating time cycles, e.g. R/)
- Exclusive Gateway
- Parallel Gateway
- Script Task (Go)
- User Task
- None End Event

## License
Apache License, Version 2.0

## Copyright
Copyright 2023 Quantek Systems Inc.

[https://www.quanteksystems.com](https://www.quanteksystems.com)