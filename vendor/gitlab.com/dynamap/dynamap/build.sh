cd ./builder/cmd/dynamap
env GOOS=windows go build -o ../../../bin/win/dynamap.exe
env GOOS=linux go build -o ../../../bin/linux/dynamap
env GOOS=darwin go build -o ../../../bin/mac/dynamap