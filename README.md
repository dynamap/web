# DynaMap™ Web
The dynamap web project enables creating web services for dynamap BPMN processes.
The goal is to enable out of the box web services for an executable BPMN process. Additionally,
 it should enable customization as needed for specific processes with additional backend, debugging, and other end points.

It provides backend APIs for the following:
- Starting process instances
- Listing running process instances
- Listings available tasks
- Completing tasks

It also provides a simple debug web interface for the following:
- Listing running process instances
- Listing available tasks
- Limited support for completing tasks

## Dependencies
- Golang 1.20: [https://go.dev](https://go.dev)
- DynaMap: `go get gitlab.com/dynamap/web@latest`
- goimports: `go install golang.org/x/tools/cmd/goimports@latest`

## Getting Started
DynaMap™ web uses a command line tool to generate the go code for a dynamap web service. From a new empty Go project:

1. Create a BPMN workflow model
2. Get dynamap web if not already installed:
```shell
go get gitlab.com/dynamap/web@latest
```
3. Install the dynamap service `dynamapsrv` command:
```shell
go install gitlab.com/dynamap/web/app/cmd/dynamapsrv@latest
```
4. From the root of a dynamap project:
```bash
dynamapsrv init
```
5. Run go mod tidy:
```bash
go mod tidy
```
6. Start the web service for the process:
```bash
go run app\service\main.go
```
The service listens by default on `localhost:8081`

## REST API
The following endpoints are exposed by default for each process definition id:
- Start a process instance: `[POST]/[process definition id]/start`
- Get a list of current process instances: `[GET]/[process definition id]/processinstances`
- Get a list of tasks: `[GET]/[process definition id]/tasks`
- Complete a task `[POST]/[process definition id]/tasks/complete`
- Debug page listing process instances: `[GET]/[process definition id]/debug/processinstances`
- Debug page listing tasks: `[GET]/[process definition id]/debug/tasks`

## Examples
Detailed examples may be found in the examples project:

[https://gitlab.com/dynamap/examples](https://gitlab.com/dynamap/examples)

## License
Apache License, Version 2.0